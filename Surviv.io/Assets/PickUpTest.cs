﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpTest : MonoBehaviour
{
    private InventoryTest testinvent;
    public GameObject weaponButton;

    private void Start()
    {
        testinvent = GameObject.FindGameObjectWithTag("Player").GetComponent<InventoryTest>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            for(int i = 0; i < testinvent.slots.Length; i++)
            {
                if(testinvent.isFull[i] == false)
                {
                    testinvent.isFull[i] = true;
                    Instantiate(weaponButton,testinvent.slots[i].transform,false);
                    Destroy(gameObject);
                    break;
                }
            }
        }
    }
}
