﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System;

public class AiManager : MonoBehaviour
{
    private AIDestinationSetter playerChaseBehavior;
    private Patrol patrolBehavior;

    public Action fire;

    // Start is called before the first frame update
    void Start()
    {
        playerChaseBehavior = GetComponent<AIDestinationSetter>();
        patrolBehavior = GetComponent<Patrol>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy") || collision.gameObject.tag.Equals("Player"))
        {
            playerChaseBehavior.enabled = true;
            patrolBehavior.enabled = false;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy") || collision.gameObject.tag.Equals("Player"))
        {
            playerChaseBehavior.enabled = false;
            patrolBehavior.enabled = true;
        }
    }

}
