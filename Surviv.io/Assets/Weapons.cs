﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapons : MonoBehaviour
{
    public enum WeaponType
    {
        Pistol,
        Shotgun,
        Rifle
    }

    private WeaponType weaponType;
    private int ammo;
    private int damage;


    public WeaponType GetWeaponType( WeaponType nWeaponType)
    {
        return nWeaponType;
    }

    public int GetAmmo(int nAmmo)
    {
        return nAmmo;
    }

    public int GetMaxAmmo()
    {
        switch (weaponType)
        {
            default: 
                return 0;
            case WeaponType.Pistol:
                return 15;
            case WeaponType.Shotgun:
                return 2;
            case WeaponType.Rifle:
                return 30;
        }
    }

    public bool CanReload()
    {
        return ammo < GetMaxAmmo();
    }

    public virtual void Reload()
    {
        ammo = GetMaxAmmo();
    }

    public int GetDamage()
    {
        switch (weaponType)
        {
            default:
                return 0;
            case WeaponType.Pistol:
                return 10;
            case WeaponType.Shotgun:
                return 20;
            case WeaponType.Rifle:
                return 15;
        }
    }

    public bool SpendAmmo()
    {
        if(ammo > 0)
        {
            ammo--;
            return true;
        }
        else
        {
            return false;
        }
    }

    public virtual void Fire()
    {
        SpendAmmo();
        // Override Instatiation of bullets in weapons
    }
}
