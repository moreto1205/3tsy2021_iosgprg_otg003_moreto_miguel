﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HP))]
public class Unit : MonoBehaviour
{
    public HP hp;
    public float speed;

    public Weapons primary;
    public Weapons secondary;
    public Weapons temp;

    public Transform firepoint;

    public int[] ammo;
    //0 pistol
    // 1 shot
    // 2 AR

    public int rifleAmmo;
    public int shotgunAmmo;
    public int pistolAmmo;


    public virtual void Init(float mHp, float mSpeed)
    {
        hp = this.GetComponent<HP>();
        this.speed = mSpeed;

        hp.Init(mHp);
    }

    public void DamageTaken(float dmg)
    {
        hp.TakeDamage(dmg);
    }

    public void shoot()
    {
        primary.Fire(firepoint);
        Debug.Log("firing");
        // calls fire from weapon class
    }

    public void reload()
    {
        //primary.curAmmo

        //Debug.Log("Reloading");
        // identify weapon by checking class

        //if(primary is Pistol)
        // Acess the gun and get current amount of ammo add to the overall ammo of the type
        // Make the current ammo 0
        // pistolAmmo - Maxsize deduct clip size
        // call primary.reload last

    }

    void Start()
    {

    }

    void Update()
    {

    }
}
