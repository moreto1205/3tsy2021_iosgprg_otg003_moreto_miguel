﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Weapons
{
    public Transform Firepoint;
    public GameObject bulletprefab;
    public GameObject player;
    public float bulletforce;

    int bulletNum = 8;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Fire(Transform mFire)
    {
        base.Fire(mFire);
        for (int i = 0; i < bulletNum; i++)
        {
            //shotgun
            var tempBullet = (GameObject)Instantiate(bulletprefab, mFire.position, mFire.rotation);
            // Gets Rigidbody2D compoenent from spawned bullet
            Rigidbody2D tempBulletRB = tempBullet.GetComponent<Rigidbody2D>();

            // Randomize angle variation between bullets
            float spreadAngle = Random.Range(-10, 10);

            // Take the random angle variation and add it to the initial
            // desiredDirection (which we convert into another angle), which in this case is the players aiming direction
            var x = mFire.position.x - player.transform.position.x;
            var y = mFire.position.y - player.transform.position.y;
            float rotateAngle = spreadAngle + (Mathf.Atan2(y, x) * Mathf.Rad2Deg);

            // Calculate the new direction we will move in which takes into account 
            // the random angle generated
            var MovementDirection = new Vector2(Mathf.Cos(rotateAngle * Mathf.Deg2Rad), Mathf.Sin(rotateAngle * Mathf.Deg2Rad)).normalized;

            tempBulletRB.velocity = MovementDirection * bulletforce;
        }

         //https://www.reddit.com/r/Unity2D/comments/76dyvl/how_do_you_do_a_shotgun_spread_in_a_topdown/
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (collision.GetComponent<PlayerStats>().weaponCount == 0)
            { 
                this.transform.parent = collision.transform;
                transform.localRotation = Quaternion.identity;
                transform.localPosition = Vector3.zero;
                //transform.localScale = Vector3.one;
                this.GetComponent<SpriteRenderer>().enabled = false;
                collision.GetComponent<PlayerStats>().primary = this;
                collision.GetComponent<PlayerStats>().weaponCount = 1;
            }

            else if (collision.GetComponent<PlayerStats>().weaponCount == 1)
            {
                this.transform.parent = collision.transform;
                transform.localRotation = Quaternion.identity;
                transform.localPosition = Vector3.zero;
                //transform.localScale = Vector3.one;
                this.GetComponent<SpriteRenderer>().enabled = false;
                collision.GetComponent<PlayerStats>().secondary = this;
                collision.GetComponent<PlayerStats>().weaponCount = 2;
            }
        }
    }

   
}
