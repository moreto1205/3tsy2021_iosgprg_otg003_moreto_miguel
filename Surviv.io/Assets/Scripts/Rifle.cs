﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rifle : Weapons
{

    public GameObject bulletPrefab;

    public Transform firepoint;

    public float bulletSpeed;

    public float firerate;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Fire(Transform mFire)
    {
        if (Input.GetButtonDown("Shoot"))
        { 
            GameObject bullet = Instantiate(bulletPrefab, mFire.position, mFire.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(mFire.up * bulletSpeed, ForceMode2D.Impulse);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (collision.GetComponent<PlayerStats>().weaponCount == 0)
            {
                this.transform.parent = collision.transform;
                transform.localRotation = Quaternion.identity;
                transform.localPosition = Vector3.zero;
                this.GetComponent<SpriteRenderer>().enabled = false;
                collision.GetComponent<PlayerStats>().primary = this;
                collision.GetComponent<PlayerStats>().weaponCount = 1;
            }

            else if (collision.GetComponent<PlayerStats>().weaponCount == 1)
            {
                this.transform.parent = collision.transform;
                transform.localRotation = Quaternion.identity;
                transform.localPosition = Vector3.zero;
                this.GetComponent<SpriteRenderer>().enabled = false;
                collision.GetComponent<PlayerStats>().secondary = this;
                collision.GetComponent<PlayerStats>().weaponCount = 2;
            }
        }
    }


}

// On pointer down repeatable call of fire
// Use either update time / invoke repeating / coroutine

// On pointer up cancel all the update/invoke/coroutine
