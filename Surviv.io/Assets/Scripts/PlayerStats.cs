﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class PlayerStats : Unit
{
    private Rigidbody2D rb;

    public Transform dropPoint;

    public int weaponCount;

    public int damageApplied;

    public Action PlayerDeath;


    [SerializeField] private Image Ppistol;
    [SerializeField] private Image Spistol;
    [SerializeField] private Image PShot;
    [SerializeField] private Image SShot;
    [SerializeField] private Image PAr;
    [SerializeField] private Image SAr;

    // Start is called before the first frame update

    void Start()
    {
        Init(100f, 10f);
        rb = GetComponent<Rigidbody2D>();
        //damageApplied =GetComponent<Bullet>().damage;
        //PlayerDeath += GameManager
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("HP TAKE DAMAGE");
            DamageTaken(10);
           
            if (hp.CurHp <= 0)
            {
                Destroy(gameObject);
                playerDeath();
            }
        }

        weaponUiCheck();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "bullet")
        {
            DamageTaken(20);

            if (hp.CurHp <= 0)
            {
                Destroy(gameObject);
                playerDeath();
            }
        }
    }

    void playerDeath()
    {
        SceneManager.LoadScene("Restart");
        //load restart menu
    }

    
    public void drop()
    {
        temp = secondary;
        primary.drop();
        primary = temp;
        secondary = null;
        weaponCount --;
    }

    public void swapGun()
    {
        temp = primary;
        primary = secondary;
        secondary = temp;
    }

    public void suicide()
    {
        SceneManager.LoadScene("Restart");
    }

    public void weaponUiCheck()
    {
        if (primary is Pistol)
        {
            Ppistol.enabled = true;
            PShot.enabled = false;
            PAr.enabled = false;

        }

        if (primary is Shotgun)
        {
            PShot.enabled = false;
            PShot.enabled = true;
            PAr.enabled = false;

        }

        if (primary is Rifle)
        {
            PAr.enabled = false;
            PShot.enabled = false;
            PAr.enabled = true;

        }

        if (secondary is Pistol)
        {
            Spistol.enabled = true;
            SShot.enabled = false;
            SAr.enabled = false;
        }

        if (secondary is Shotgun)
        {
            SShot.enabled = false;
            SShot.enabled = true;
            SAr.enabled = false;
        }

        if (secondary is Rifle)
        {
            SAr.enabled = false;
            SShot.enabled = false;
            SAr.enabled = true;
        }

        if (primary is null)
        {
            Ppistol.enabled = false;
            PShot.enabled = false;
            PAr.enabled = false;
        }

        if(secondary is null)
        {
            Spistol.enabled = false;
            SShot.enabled = false;
            SAr.enabled = false;
        }

    }


}
