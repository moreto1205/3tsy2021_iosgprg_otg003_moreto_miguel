﻿using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    float Xmove = 0f;
    float Ymove = 0f;
    private Rigidbody2D rb;
    Vector2 movement;
    public float speed = 2f;

    public Joystick jt;
    public Transform firepoint;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Xmove = SimpleInput.GetAxis("Horizontal");
        Ymove = SimpleInput.GetAxis("Vertical");

        rb.velocity = new Vector2(Xmove * speed, Ymove * speed);

        transform.position = new Vector2(Mathf.Clamp(transform.position.x, -27.92825f, 29.03381f),
            Mathf.Clamp(transform.position.y, -22.16242f, 22.04932f));

        Vector3 moveVector = (Vector3.up * jt.Vertical - Vector3.left * jt.Horizontal);
        if (jt.Horizontal != 0 || jt.Vertical != 0)
        {
            transform.rotation = Quaternion.LookRotation(Vector3.forward, moveVector);
        }

    }


   

    // create Ammo class under Inventory script

}



//    // Pickup system 
//    // fire system
//    // adjust sizing
//    // array attached to player for guns only
//    // array attached to player for ammo only 


//if (collision.gameObject.tag == "Rifle")
//{
//    SetCanUseRifle();
//    Destroy(collision.gameObject);

//    if (canUseRifle == true)
//    {
//        rifleAmmo += 15;
//    }
//}

//if (collision.gameObject.tag == "Shotgun")
//{
//    SetCanUseShotgun();
//    Destroy(collision.gameObject);

//    if (canUseShotgun == true)
//    {
//        shotgunAmmo += 2;
//    }
//}

//if (collision.gameObject.tag == "Pistol")
//{
//    SetCanUsePistol();
//    Destroy(collision.gameObject);

//    if (canUsePistol == true)
//    {
//        pistolAmmo += 10;
//    }
//}


//public void SetCanUseShotgun()
//{
//    canUseShotgun = true;
//    SetWeapon(wShotgun);
//}

//public void SetCanUseRifle()
//{
//    canUseRifle = true;
//    SetWeapon(wRifle);
//}

//public void SetCanUsePistol()
//{
//    canUsePistol = true;
//    SetWeapon(wPistol);
//}
//private Weapons wPistol;
//private Weapons wShotgun;
//private Weapons wRifle;

//private bool canUseShotgun;
//private bool canUseRifle;
//private bool canUsePistol;