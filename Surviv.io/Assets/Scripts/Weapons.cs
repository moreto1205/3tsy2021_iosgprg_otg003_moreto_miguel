﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapons : MonoBehaviour
{
    //private WeaponType weaponType;
    public int damage;
    public int weaponId;
    // 0 pistol
    // 1 shottie
    // 2 ar

    public int MaxAmmo;
    public int reloadtime = 1;
    public int curAmmo;

    bool isReloading;

     void Start()
    {
        if(curAmmo == -1)
        {
            curAmmo = MaxAmmo;
        }
    }
    public void Init(int mammosize, int mdamage)
    {
        this.MaxAmmo = mammosize;
        this.damage = mdamage;
    }

    public virtual void Fire(Transform mFire) //int curAmmo
    {
        curAmmo--;
        // Override Instatiation of bullets in weapons
    }

   public void reload(int clipToLoad)
    {

        StartCoroutine(reloading(clipToLoad));
        return;
    }

    IEnumerator reloading(int clipToLoad)
    {
        yield return new WaitForSeconds(reloadtime);
        curAmmo = clipToLoad;
    }

    public void drop()
    {
        transform.position = transform.parent.GetComponent<PlayerStats>().dropPoint.position;
        transform.parent.GetComponent<PlayerStats>().primary = null;
        transform.parent = null;
        this.GetComponent<SpriteRenderer>().enabled = true;
    }
}


//public WeaponType GetWeaponType( WeaponType nWeaponType)
//{
//    return nWeaponType;
//}

//public int GetAmmo(int nAmmo)
//{
//    return nAmmo;
//}

//public int GetMaxAmmo()
//{
//    switch (weaponType)
//    {
//        default: 
//            return 0;
//        case WeaponType.Pistol:
//            return 15;
//        case WeaponType.Shotgun:
//            return 2;
//        case WeaponType.Rifle:
//            return 30;
//    }
//}

//public bool CanReload()
//{
//    return ammo < GetMaxAmmo();
//}

//public virtual void Reload()
//{
//    ammo = GetMaxAmmo();
//}

//public int GetDamage()
//{
//    switch (weaponType)
//    {
//        default:
//            return 0;
//        case WeaponType.Pistol:
//            return 10;
//        case WeaponType.Shotgun:
//            return 20;
//        case WeaponType.Rifle:
//            return 15;
//    }
//}

//public bool SpendAmmo()
//{
//    if(ammo > 0)
//    {
//        ammo--;
//        return true;
//    }
//    else
//    {
//        return false;
//    }
//}

//public enum WeaponType
//{
//    Pistol,
//    Shotgun,
//    Rifle
//}