﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Weapons
{
    public GameObject bullet;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Fire()
    {
        base.Fire();
        Instantiate(bullet, this.transform.position, Quaternion.identity);
        //Instantiate bullet
    }
}
