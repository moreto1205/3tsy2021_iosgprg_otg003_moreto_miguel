﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class enemyStats : Unit
{
    public float rate;

    int damageTake;

    // Start is called before the first frame update
    void Start()
    {
        Init(100, 10f);
        this.GetComponentInParent<AIDestinationSetter>().onShoot += shoot;
        damageTake = GetComponent<Bullet>().damage;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "bullet")
        {
            DamageTaken(50);

            if(hp.CurHp <=0)
            {
                Destroy(gameObject);
            }
        }
    }


    // invoke repeating fire if uniot gfets close and disable when away

}
