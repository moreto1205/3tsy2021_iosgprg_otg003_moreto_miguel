﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class Player : MonoBehaviour
{
    float Xmove = 0f;
    float Ymove = 0f;
    private Rigidbody2D rb;
    Vector2 movement;
    public float speed = 2f;

    public float hp = 100;

    float rotate = 0f;

    public Joystick jt;

    bool isFiring;

    public Weapons weapon;
    private Weapons wPistol;
    private Weapons wShotgun;
    private Weapons wRifle;

    private bool canUseShotgun;
    private bool canUseRifle;
    private bool canUsePistol;

    private int rifleAmmo;
    private int shotgunAmmo;
    private int pistolAmmo;

    public GameObject bullet;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Xmove = SimpleInput.GetAxis("Horizontal");
        Ymove = SimpleInput.GetAxis("Vertical");

        rotate = jt.Horizontal * -1f;

        rb.velocity = new Vector2(Xmove * speed, Ymove * speed);
        rb.transform.Rotate(0, 0, rotate);

        if (Input.GetKeyUp(KeyCode.Space))
        {
            shoot();
        }

        weapon.GetAmmo(rifleAmmo);

    }
    public void SetWeapon(Weapons weapon)
    {
        this.weapon = weapon;
    }

    public void SetCanUseShotgun()
    {
        canUseShotgun = true;
        SetWeapon(wShotgun);
    }

    public void SetCanUseRifle()
    {
        canUseRifle = true;
        SetWeapon(wRifle);
    }

    public void SetCanUsePistol()
    {
        canUsePistol = true;
        SetWeapon(wPistol);
    }
    public void shoot()
    {
        weapon.Fire();
        Debug.Log("firing");
        AmmoCount.amount -= 1;
        // calls fire from weapon class
    }

    public void reload()
    {
        weapon.Reload();
        AmmoCount.amount = 30;
        Debug.Log("Reloading");
    }

    // create Ammo class under Inventory script

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Rifle Bullets")
        {
            rifleAmmo += 30;
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.tag == "9mm Bullets")
        {
            pistolAmmo += 15;
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.tag == "Shotgun Shells")
        {
            shotgunAmmo += 4;
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.tag == "Rifle")
        {
            SetCanUseRifle();
            Destroy(collision.gameObject);

            if (canUseRifle == true)
            {
                rifleAmmo += 15;
            }
        }

        if (collision.gameObject.tag == "Shotgun")
        {
            SetCanUseShotgun();
            Destroy(collision.gameObject);

            if (canUseShotgun == true)
            {
                shotgunAmmo += 2;
            }
        }

        if (collision.gameObject.tag == "Pistol")
        {
            SetCanUsePistol();
            Destroy(collision.gameObject);

            if (canUsePistol == true)
            {
                pistolAmmo += 10;
            }
        }


    }
}

//    private void OnTriggerEnter2D(Collider2D collision)
//    {
//        if (collision.gameObject.tag == "Pistol")
//        {
//            SetCanUsePistol();

//            // set parent to add pistol to reparent to player 1
//            // disable sprite and reset position to origin of the player 2
//            // use sprite renderer enable 
//            // enable and disable component of gameobject for sprite renderer

//            if (canUsePistol == true)
//            {
//                pistolAmmo += 10;
//            }
//        }
//    }
//    // Pickup system 
//    // fire system
//    // adjust sizing
//    // array attached to player for guns only
//    // array attached to player for ammo only 
//}
