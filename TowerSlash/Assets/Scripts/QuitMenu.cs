﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuitMenu : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene("Main", LoadSceneMode.Single);
    }

    public void CharSelect()
    {
        SceneManager.LoadScene("Character Select", LoadSceneMode.Single);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
