﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class movement : MonoBehaviour
{
    public float speed;
    public int health;
    public float dashspeed;
    private int direction;
    private Rigidbody2D rb;
    private Vector2 startTouch, endTouch;
    private bool dashAllow = false;
    private int FakeHp = 1000;
    private int HpStorage;


    //powerup dash
    public float dashForce;
    public float StartdashTimer;
    bool isDashing = false;
    float CurrentdashTimer;
    float dashDirection;

    void Start()
    {
        HpUI.Amount += health;
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        //this.transform.position += transform.right * speed * Time.deltaTime;
        TapCheck();
    }

    private void FixedUpdate()
    {
        Dash();
        DashUp();
    }
    private void TapCheck()
    {
        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            startTouch = Input.GetTouch(0).position;
            dashAllow = true;
        }

        else if(Input.touchCount > 0 && Input.GetTouch(0).phase ==TouchPhase.Moved)
        {
            dashAllow = false;
        }

        else if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            endTouch = Input.GetTouch(0).position;
            dashAllow = false;
        }   
    }

    private void Dash()
    {
        if(dashAllow == true)
        {
            rb.velocity = Vector2.up * dashspeed;
        }
    }
    private void DashUp()
    {

        if (isDashing == true)
        {
            HpStorage = health;
            health = FakeHp;
            rb.velocity = Vector2.up * dashForce;
            CurrentdashTimer -= Time.deltaTime;

            if (CurrentdashTimer <= 0)
            {
                health = HpStorage;
                isDashing = false;
            }
        }

        else if (isDashing == false)
        {
            rb.velocity = Vector2.up / dashForce;
            dashAllow = true;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            health--;
            HpUI.Amount -= 1;

            if (health <= 0)
            {
                Destroy(gameObject);
                SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
            }
        }

        else if (collision.gameObject.tag.Equals("HpUp"))
        {
            health += 1 ;
            HpUI.Amount += 1;
        }

        else if(collision.gameObject.tag.Equals("DashUp"))
        {
            Score.Scores += 100;
            isDashing = true;
            dashAllow = false;
            CurrentdashTimer = StartdashTimer;
        }
    }

}

    //if (Input.GetKeyDown(KeyCode.Space) & direction == 0)
    //    {
    //        direction = 1;
    //        if (direction == 1)
    //        {
    //            rb.velocity = Vector2.up* dashspeed;
    //        }
    //    }
    //    else if (Input.GetKeyUp(KeyCode.Space))
    //    {
    //        rb.velocity = Vector2.up / dashspeed;
    //        direction = 0;
    //    }