﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashUp : MonoBehaviour
{
    private Rigidbody2D rb;
    public float moveSpeed = 1f;
    private float cameraY;
    // Start is called before the first frame update
    void Start()
    {
        cameraY = Camera.main.transform.position.y - 10f;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        move();
        deactivate();
    }

    void move()
    {
        Vector3 temp = transform.position;
        temp.y -= moveSpeed * Time.deltaTime;
        transform.position = temp;
    }

    void deactivate()
    {
        if (transform.position.y < cameraY)
        {
            gameObject.SetActive(false);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            Destroy(gameObject);
        }
    }
}
