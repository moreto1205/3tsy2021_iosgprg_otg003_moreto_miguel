﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Vector3 offset;
    private GameObject player;
    

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (player == null)
        { 
            player = GameObject.FindGameObjectWithTag("Player");
        }

        else
        {
            transform.position = player.transform.position + offset;
        }
    }


    //int PSelect = PlayerPrefs.GetInt("selected");
    //if (player == null)
    //{
    //   if (PSelect == 1)
    //    {
    //        player = GameObject.Find("Real Player(Clone)");
    //    }

    //   else if (PSelect == 2)
    //    {
    //        player = GameObject.Find("Real Player 2(Clone)");
    //    }

    //   else if(PSelect == 3)
    //    {
    //        player = GameObject.Find("Real Player 3(Clone)");
    //    }
    //}
}
