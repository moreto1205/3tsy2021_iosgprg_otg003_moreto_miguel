﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arrowRotate : MonoBehaviour
{

    public float rotationSpeed;
    public int target;
  
    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void Update()
    {

        if (transform.eulerAngles.z < target)
        {
            transform.Rotate(new Vector3(0f, 0f, rotationSpeed));
        }

        //var totalRotTime = 5f;
        //var DegreesPerSecond = transform.eulerAngles.y / totalRotTime;

        //transform.Rotate(new Vector3(0, 0, DegreesPerSecond * Time.deltaTime));
        //rb.rotation += 90;


    }
}
