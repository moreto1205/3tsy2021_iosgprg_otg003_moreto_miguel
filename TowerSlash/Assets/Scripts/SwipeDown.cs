﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class SwipeDown : MonoBehaviour
{
    private Rigidbody2D rb;
    private Vector2 startTouch, endTouch;
    private bool kill = false;

    public float moveSpeed = 1f;
    private float cameraY;
    // Start is called before the first frame update
    void Start()
    {
        cameraY = Camera.main.transform.position.y - 10f;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        move();
        Swipe();
        Keys();
        deactivate();
        killEnemy();
    }

    //private void FixedUpdate()
    //{
    //    killEnemy();
    //}

    void move()
    {
        Vector3 temp = transform.position;
        temp.y -= moveSpeed * Time.deltaTime;
        transform.position = temp;
    }

    void deactivate()
    {
        if (transform.position.y < cameraY)
        {
            gameObject.SetActive(false);
        }
    }
    private void Keys()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            Destroy(gameObject);
        }
    }

    private void Swipe()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            startTouch = Input.GetTouch(0).position;
        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            endTouch = Input.GetTouch(0).position;

            if (endTouch.x > startTouch.x)
            {
                kill = false;
            }
            else if (endTouch.x < startTouch.x)
            {
                kill = false;
            }
            else if (endTouch.y > startTouch.y)
            {
                kill = false;
            }
            else if (endTouch.y < startTouch.y)
            {
                kill = true;
            }
        }
    }

    private void killEnemy()
    {
        if (kill == true)
        {
            Debug.Log("DownSwipe");
            Destroy(gameObject);
            Score.Scores += 10;
        }
    }
}
