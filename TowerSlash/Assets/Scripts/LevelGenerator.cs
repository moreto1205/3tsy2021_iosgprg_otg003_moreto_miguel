﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public float moveSpeed;
    private GameObject[] Blocks;
    private float cameraY;
    private float Height;

    public GameObject[] enemies;
    public GameObject[] spawnPos;   

    private void Awake()
    {
        Blocks = GameObject.FindGameObjectsWithTag("Blocks");
        cameraY = Camera.main.gameObject.transform.position.y;
        Height = GetComponent<BoxCollider2D>().bounds.size.y;
    }

    void Update()
    {
        move();
        Reposition();
    }

    void move()
    {
        Vector3 temp = transform.position;
        temp.y -= moveSpeed * Time.deltaTime;
        transform.position = temp;
    }

    void Reposition()
    {
        if (transform.position.y < cameraY)
        {
            float HighestBlocksY = Blocks[0].transform.position.y;

            for(int i = 1; i < Blocks.Length; i++)
            {
                if (HighestBlocksY < Blocks[i].transform.position.y)
                {
                    HighestBlocksY = Blocks[i].transform.position.y;
                }
            }

            Vector3 temp = transform.position;
            temp.y = HighestBlocksY + Height;
            transform.position = temp;


            spawnEnemy();    
        }
    }

    void spawnEnemy()
    {
        if (Random.Range(0,10) > 0)
        {
            int randomEnemyIndex = Random.Range(0, enemies.Length);

            Instantiate(enemies[randomEnemyIndex], new Vector3(5.5f, transform.position.y, 9f), Quaternion.identity);    
        }
    }
}

// 5.5 normal
// 8.15 opposite


// private const float PlayerDistance = 150f;

//[SerializeField] private Transform LevelStart;
//[SerializeField] private Transform Level_1;

//private Vector3 lastEndPos;

//private void Awake()
//{
//    Transform lastLevelPartTransform;
//    lastLevelPartTransform = SpawnLevelPart(LevelStart.Find("EndPosition").position);
//    lastLevelPartTransform = SpawnLevelPart(lastLevelPartTransform.Find("EndPosition").position);
//    lastLevelPartTransform = SpawnLevelPart(lastLevelPartTransform.Find("EndPosition").position);
//    lastLevelPartTransform = SpawnLevelPart(lastLevelPartTransform.Find("EndPosition").position);
//}


//private Transform SpawnLevelPart(Vector3 spawnPos)
//{
//    Transform levelPartTransform = Instantiate(Level_1, spawnPos, Quaternion.identity);
//    return levelPartTransform;
//}