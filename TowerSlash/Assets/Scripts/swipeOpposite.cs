﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class swipeOpposite : MonoBehaviour
{
    private int direction;
    private int rotation;
    private Rigidbody2D rb;
    private Vector2 startTouch, endTouch;
    private bool killUp = false;
    private bool killDown = false;
    private bool killLeft = false;
    private bool killRight = false;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        SwipeCheck();
    }

    private void FixedUpdate()
    {
        kill();
    }

    private void SwipeCheck()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            startTouch = Input.GetTouch(0).position;
        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            endTouch = Input.GetTouch(0).position;

            if (endTouch.x < startTouch.x)
            {
                killRight = true;
            }
            if (endTouch.x > startTouch.x)
            {
                killLeft = true;
            }
            if (endTouch.y < startTouch.y)
            {
                killUp = true;
            }
            if (endTouch.y > startTouch.y)
            {
                killDown = true;
            }
        }

    }

    private void kill()
    {
        direction = Random.Range(1, 4);

        if (direction == 1 && killUp == true)
        {
            Debug.Log("UpSwipe/Opposite");
            Destroy(gameObject);
        }

        if (direction == 2 && killDown == true)
        {
            Debug.Log("DownSwipe/Opposite");
            Destroy(gameObject);
        }

        if (direction == 3 && killLeft == true)
        {
            Debug.Log("LeftSwipe/Opposite");
            Destroy(gameObject);
        }

        if (direction == 4 && killRight == true)
        {
            Debug.Log("RightSwipe/Opposite");
            Destroy(gameObject);
        }
    }
}
      