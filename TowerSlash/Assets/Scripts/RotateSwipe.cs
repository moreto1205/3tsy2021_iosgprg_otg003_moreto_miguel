﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSwipe : MonoBehaviour
{
    private int direction;
    private int rotation;
    private Rigidbody2D rb;
    private Vector2 startTouch, endTouch;
    private bool killUp = false;
    private bool killDown = false;
    private bool killLeft = false;
    private bool killRight = false;


    public float moveSpeed = 1f;
    private float cameraY;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        cameraY = Camera.main.transform.position.y - 10f;
    }

    // Update is called once per frame
    void Update()
    {
        move();
        deactivate();
        SwipeCheck();
    }

    private void FixedUpdate()
    {
        kill();
    }

    void move()
    {
        Vector3 temp = transform.position;
        temp.y -= moveSpeed * Time.deltaTime;
        transform.position = temp;
    }

    void deactivate()
    {
        if (transform.position.y < cameraY)
        {
            gameObject.SetActive(false);
        }
    }
    private void SwipeCheck()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            startTouch = Input.GetTouch(0).position;
        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            endTouch = Input.GetTouch(0).position;

            if (endTouch.x > startTouch.x)
            {
                killRight = true;
            }
            if (endTouch.x < startTouch.x)
            {
                killLeft = true;
            }
            if (endTouch.y > startTouch.y)
            {
                killUp = true;
            }
            if (endTouch.y < startTouch.y)
            {
                killDown = true;
            }
        }
    }

    private void kill()
    {
        direction = Random.Range(1, 4);

        if (direction == 1 && killUp == true)
        {
            Debug.Log("UpSwipe");
            Destroy(gameObject);
            Score.Scores += 10;

        }

        if (direction == 2 && killDown == true)
        {
            Debug.Log("DownSwipe");
            Destroy(gameObject);
            Score.Scores += 10;
        }

        if (direction == 3 && killLeft == true)
        {
            Debug.Log("LeftSwipe");
            Destroy(gameObject);
            Score.Scores += 10;
        }

        if (direction == 4 && killRight == true)
        {
            Debug.Log("RightSwipe");
            Destroy(gameObject);
            Score.Scores += 10;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            Destroy(gameObject);
        }
    }
}
